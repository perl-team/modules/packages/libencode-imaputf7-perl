libencode-imaputf7-perl (1.05-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove TANIGUCHI Takaki from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Add patch to fix test warnings with perl 5.40. (Closes: #1078061)
  * Add /me to Uploaders.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Aug 2024 23:30:56 +0200

libencode-imaputf7-perl (1.05-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 14:44:27 +0100

libencode-imaputf7-perl (1.05-2) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Damyan Ivanov ]
  * remove libtest-nowarnings-perl from Depends
  * add a patch adding 'use Encode::IMAPUTF7;' to the SYNOPSIS part of the
    documentation. Closes: #642819
  * debian source format 3.0 (quilt)
  * Calim conformance to Policy 3.9.2

 -- Damyan Ivanov <dmn@debian.org>  Sun, 25 Sep 2011 15:40:26 +0300

libencode-imaputf7-perl (1.05-1) unstable; urgency=low

  * Initial Release. (Closes: #611421)

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 04 Feb 2011 13:49:09 +0900
